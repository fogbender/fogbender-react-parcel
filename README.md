this example is using [fogbender-react](https://www.npmjs.com/package/fogbender-react) and built with [Parcel](https://parceljs.org/).

    git clone git@gitlab.com:fogbender/fogbender-react-parcel.git
    yarn
    yarn start

open http://localhost:1234/

make sure to follow the instructions from the Settings page to change `clientUrl` and `token` for your workspace.
